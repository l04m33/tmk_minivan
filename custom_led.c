/*
Copyright 2012 Jun Wako <wakojun@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <avr/io.h>
#include "stdint.h"
#include "led.h"
#include "action.h"
#include "action_layer.h"

#include "custom_util.h"
#include "custom_led.h"
#include "ws2812.h"


#define DEFAULT_COLOR    { .r = 0x0c, .g = 0x08, .b = 0x14 }
#define CAPS_COLOR       { .r = 0x14, .g = 0x08, .b = 0x0c }
#define OFF_COLOR        { .r = 0x00, .g = 0x00, .b = 0x00 }

static LED_TYPE default_color = DEFAULT_COLOR;
static LED_TYPE caps_color    = CAPS_COLOR;

led_t led = {
    .state = LED_ON,
    .colors = {
        DEFAULT_COLOR,
        DEFAULT_COLOR,
        DEFAULT_COLOR,
    },
    .off_color = OFF_COLOR,
};


void set_layer_leds(uint32_t layer_state)
{
    LED_TYPE led_colors[LED_COUNT];

    if (led.state == LED_ON) {
        led_colors[0] = led.colors[0];
        led_colors[1] = led.off_color;
        led_colors[2] = led.off_color;
        /* There're only 3 LEDs, so only 8 layers are supported */
        for (int8_t i = 8; i >= 0; i--) {
            if (layer_state & (1UL << i)) {
                for (int8_t j = LED_COUNT - 1; j >= 0; j--) {
                    if ((i + 1) & (1UL << j)) {
                        led_colors[j] = led.colors[j];
                    } else {
                        led_colors[j] = led.off_color;
                    }
                }
                break;
            }
        }
        ws2812_setleds(led_colors, LED_COUNT);
    } else {
        led_colors[0] = led_colors[1] = led_colors[2] = led.off_color;
        ws2812_setleds(led_colors, LED_COUNT);
    }
}


void action_led_switch(keyrecord_t *record)
{
    if (!record->event.pressed && KEY_TAPPED(record, 1)) {
        if (led.state) {
            led.state = LED_OFF;
        } else {
            led.state = LED_ON;
        }
        set_layer_leds(layer_state);
    }
}


void led_set(uint8_t usb_led)
{
    if (usb_led & (1<<USB_LED_CAPS_LOCK)) {
        led.colors[0] = led.colors[1] = led.colors[2] = caps_color;
    } else {
        led.colors[0] = led.colors[1] = led.colors[2] = default_color;
    }
    set_layer_leds(layer_state);
}
