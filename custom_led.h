#ifndef custom_led_h_INCLUDED
#define custom_led_h_INCLUDED

#include "action.h"
#include "color.h"
#include "ws2812.h"

#define LED_COUNT 3

typedef enum {
    LED_OFF = 0,
    LED_ON,
} led_state_t;

typedef struct {
    led_state_t state;
    LED_TYPE colors[LED_COUNT];
    LED_TYPE off_color;
} led_t;

extern led_t led;
void set_layer_leds(uint32_t layer_state);
void action_led_switch(keyrecord_t *record);

#endif // custom_led_h_INCLUDED

