#include "keymap.h"
#include "keyboard.h"
#include "action.h"
#include "matrix.h"
#include "suspend.h"
#include "bootloader.h"
#include "unimap_trans.h"
#if defined(__AVR__)
#   include <avr/pgmspace.h>
#endif

#include "custom_util.h"
#include "shift_paren.h"
#include "d_macro.h"
#include "penti.h"
#include "custom_led.h"


enum function_id {
    FUNC_LSHIFT_LPAREN,
    FUNC_RSHIFT_RPAREN,
    FUNC_AUTO_LBRACKET,
    FUNC_AUTO_RBRACKET,
    FUNC_AUTO_PAREN,
    FUNC_D_MACRO_RECORD,
    FUNC_D_MACRO_PLAY,
    FUNC_D_MACRO_PLAY_1,
    FUNC_D_MACRO_PLAY_2,
    FUNC_PENTI_KEY,
    FUNC_LED_SWITCH,
    FUNC_RESET,
};

enum layer_id {
    LAYER_COLEMAK_DH = 0,
    LAYER_QWERTY,
    LAYER_SPCFN,
    LAYER_SWITCHES,
    LAYER_PENTI,
};

#define AC_CTLESC      ACTION_MODS_TAP_KEY(MOD_LCTL, KC_ESC)
#define AC_CTLENT      ACTION_MODS_TAP_KEY(MOD_RCTL, KC_ENT)
#define AC_ALTBSP      ACTION_MODS_TAP_KEY(MOD_LALT, KC_BSPC)
#define AC_SPCFN       ACTION_LAYER_TAP_KEY(LAYER_SPCFN, KC_SPC)

#define AC_SWTS        ACTION_LAYER_TAP_TOGGLE(LAYER_SWITCHES)
#define AC_QWERTY      ACTION_LAYER_TOGGLE(1)
#define AC_SPCFNT      ACTION_LAYER_TOGGLE(LAYER_SPCFN)

#define AC_LSFTPRN     ACTION_FUNCTION_TAP(FUNC_LSHIFT_LPAREN)
#define AC_RSFTPRN     ACTION_FUNCTION_TAP(FUNC_RSHIFT_RPAREN)
#define AC_AUTOLBRC    ACTION_FUNCTION_TAP(FUNC_AUTO_LBRACKET)
#define AC_AUTORBRC    ACTION_FUNCTION_TAP(FUNC_AUTO_RBRACKET)
#define AC_AUTOPRN     ACTION_FUNCTION_TAP(FUNC_AUTO_PAREN)

#define AC_MREC        ACTION_FUNCTION_TAP(FUNC_D_MACRO_RECORD)
#define AC_MPLAY       ACTION_FUNCTION_TAP(FUNC_D_MACRO_PLAY)
#define AC_MPLAY1      ACTION_FUNCTION_TAP(FUNC_D_MACRO_PLAY_1)
#define AC_MPLAY2      ACTION_FUNCTION_TAP(FUNC_D_MACRO_PLAY_2)

#define AC_PENTI        ACTION_LAYER_TOGGLE(LAYER_PENTI)
#define AC_PENTI_THUMB  ACTION_FUNCTION_OPT(FUNC_PENTI_KEY, PENTI_THUMB_BIT)
#define AC_PENTI_INDEX  ACTION_FUNCTION_OPT(FUNC_PENTI_KEY, PENTI_INDEX_BIT)
#define AC_PENTI_MIDDLE ACTION_FUNCTION_OPT(FUNC_PENTI_KEY, PENTI_MIDDLE_BIT)
#define AC_PENTI_RING   ACTION_FUNCTION_OPT(FUNC_PENTI_KEY, PENTI_RING_BIT)
#define AC_PENTI_PINKY  ACTION_FUNCTION_OPT(FUNC_PENTI_KEY, PENTI_PINKY_BIT)
#define AC_PENTI_REPEAT ACTION_FUNCTION_OPT(FUNC_PENTI_KEY, PENTI_REPEAT_BIT)

#define AC_LEDSW       ACTION_FUNCTION_TAP(FUNC_LED_SWITCH)

#define AC_RESET       ACTION_FUNCTION_TAP(FUNC_RESET)

#ifndef _
#   define _ TRNS
#else
#   error "The _ macro is already defined!"
#endif

#ifdef KEYMAP_SECTION_ENABLE
const action_t actionmaps[][UNIMAP_ROWS][UNIMAP_COLS] __attribute__ ((section (".keymap.keymaps"))) = {
#else
const action_t actionmaps[][UNIMAP_ROWS][UNIMAP_COLS] PROGMEM = {
#endif

    [LAYER_COLEMAK_DH] = UNIMAP_MINIVAN(
        TAB,     Q, W, F, P, B, J, L, U,    Y,   SCLN, AUTOLBRC,
        CTLESC,  A, R, S, T, G, K, N, E,    I,   O,    CTLENT,
        LSFTPRN, X, C, D, V, Z, M, H, COMM, DOT, SLSH, RSFTPRN,
        SWTS,LGUI, ALTBSP,  SPCFN,  SPCFN,  RALT,RGUI, SWTS),

    [LAYER_QWERTY] = UNIMAP_MINIVAN(
        _, Q, W, E, R, T, Y, U, I, O, P,    _,
        _, A, S, D, F, G, H, J, K, L, SCLN, _,
        _, Z, X, C, V, B, N, M, _, _, _,    _,
        _, _, _,     _,     _,     _, _,    _),

    [LAYER_SPCFN] = UNIMAP_MINIVAN(
        GRV,  EQL, 3, 2, 1,   MINS, HOME, PGDN, PGUP, END,  DELETE, LBRC,
        LCTL, 0,   6, 5, 4,   QUOT, LEFT, DOWN, UP,   RGHT, INS,    RCTL,
        LSFT, 9,   8, 7, DOT, ENT,  RBRC, BSLS, _,    _,    _,      RSFT,
        _,    _,   _,           _,          _,        _,    _,      _),

    [LAYER_SWITCHES] = UNIMAP_MINIVAN(
        PENTI, F11, F3, F2, F1, PSCR, QWERTY, SPCFNT, AUTOPRN, LEDSW,  _,    RESET,
        _,     F10, F6, F5, F4, CAPS, MS_L,   MS_D,   MS_U,    MS_R,   BTN1, BTN2,
        _,     F12, F9, F8, F7, SLCK, MREC,   MPLAY,  MPLAY1,  MPLAY2, _,    _,
        _,     _,   _,            _,            _,             _,      _,    _),

    [LAYER_PENTI] = UNIMAP_MINIVAN(
        PENTI, NO,          PENTI_RING, PENTI_MIDDLE, PENTI_INDEX, NO, NO, NO, NO, NO, NO, NO,
        NO,    PENTI_PINKY, NO,         PENTI_REPEAT, NO,          NO, NO, NO, NO, NO, NO, NO,
        NO,    NO,          NO,         NO,           NO,          NO, NO, NO, NO, NO, NO, NO,
        _,     NO,          NO,         PENTI_THUMB,          NO,                  NO, NO, _),
};


void hook_late_init(void)
{
    auto_paren_state.enabled = 1;
    d_macro_stack_scan_hook_late_init();

    set_layer_leds(layer_state);
}


void hook_layer_change(uint32_t layer_state)
{
    set_layer_leds(layer_state);
}


void hook_matrix_change(keyevent_t event)
{
    d_macro_hook_matrix_change(event);
}


void hook_keyboard_loop(void)
{
    d_macro_stack_scan();
    d_macro_hook_keyboard_loop();
}


static LED_TYPE led_suspend_color = { .r = 0x14, .g = 0x07, .b = 0x00 };
static LED_TYPE led_orig_colors[LED_COUNT];

void hook_usb_suspend_entry(void)
{
    for (uint8_t i = 0; i < LED_COUNT; i++) {
        led_orig_colors[i] = led.colors[i];
    }
    for (uint8_t i = 0; i < LED_COUNT; i++) {
        led.colors[i] = led_suspend_color;
    }
    set_layer_leds(layer_state);

    matrix_clear();
    clear_keyboard();
}


void hook_usb_wakeup(void)
{
    suspend_wakeup_init();

    for (uint8_t i = 0; i < LED_COUNT; i++) {
        led.colors[i] = led_orig_colors[i];
    }
    set_layer_leds(layer_state);
}


static LED_TYPE led_bootloader_color = { .r = 0x1b, .g = 0x00, .b = 0x00 };

void action_reset(void)
{
    for (uint8_t i = 0; i < LED_COUNT; i++) {
        led.colors[i] = led_bootloader_color;
    }
    set_layer_leds(layer_state);

    matrix_clear();
    clear_keyboard();

    bootloader_jump();
}


void action_function(keyrecord_t *record, uint8_t id, uint8_t opt)
{
    switch (id) {
        case FUNC_LSHIFT_LPAREN:
            action_shift_paren(record, KC_LSHIFT);
            break;
        case FUNC_RSHIFT_RPAREN:
            action_shift_paren(record, KC_RSHIFT);
            break;
        case FUNC_AUTO_LBRACKET:
            action_shift_paren(record, KC_LBRACKET);
            break;
        case FUNC_AUTO_RBRACKET:
            action_shift_paren(record, KC_RBRACKET);
            break;
        case FUNC_AUTO_PAREN:
            action_auto_paren(record);
            break;
        case FUNC_D_MACRO_RECORD:
            d_macro_action_record(record);
            break;
        case FUNC_D_MACRO_PLAY:
            d_macro_action_play(record, -1);
            break;
        case FUNC_D_MACRO_PLAY_1:
            d_macro_action_play(record, 1);
            break;
        case FUNC_D_MACRO_PLAY_2:
            d_macro_action_play(record, 50);
            break;
        case FUNC_PENTI_KEY:
            action_penti_key(record, opt);
            break;
        case FUNC_LED_SWITCH:
            action_led_switch(record);
            break;
        case FUNC_RESET:
            action_reset();
            break;
        default:
            break;
    }
}


extern keypos_t unimap_translate(keypos_t key);

action_t action_for_key(uint8_t layer, keypos_t key)
{
    keypos_t uni = unimap_translate(key);
    if ((uni.row << 4 | uni.col) == UNIMAP_NO) {
        return (action_t)ACTION_NO;
    }

    action_t action =
#if defined(__AVR__)
        (action_t)pgm_read_word(&actionmaps[(layer)][(uni.row & 0x7)][(uni.col)]);
#else
        actionmaps[(layer)][(uni.row & 0x7)][(uni.col)];
#endif

    return d_macro_action_for_key(key, action);
}
