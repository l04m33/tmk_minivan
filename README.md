# TMK Firmware for the MiniVan Kumo Keyboard #

This is the firmware for my own MiniVan Kumo (The latest PCB with 3
on-board LEDs). I didn't want to check out the whole QMK source repo,
so I created this. Please consult `unimap_l04m33.c` for detailed
features.

The LED driver and color related codes are ripped from the QMK
firmware.

## License ##

GPLv2+
